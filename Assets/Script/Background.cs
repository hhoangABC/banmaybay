using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] protected Renderer meshRender;
    [SerializeField] protected float speed = 0.2f;

    protected void Update()
    {
        meshRender.material.mainTextureOffset += new Vector2(0, speed * Time.deltaTime);
    }
}
