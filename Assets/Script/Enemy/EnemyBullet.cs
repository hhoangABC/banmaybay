﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [SerializeField] protected float bulletSpeed = -10;

    [SerializeField] protected float despawnDistance = -20;
    protected float traveledDistance = 0;
    protected Transform spawnBullett;

    protected void Update()
    {
        transform.Translate(Vector2.up * bulletSpeed * Time.deltaTime);

        //Huỷ viên đạn
        traveledDistance += bulletSpeed * Time.deltaTime;
        if (traveledDistance <= despawnDistance)
        {
            Destroy(gameObject);
        }
    }
}
