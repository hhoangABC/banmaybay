using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected float enemySpeed;

    protected void Update()
    {
        transform.Translate(Vector2.down * enemySpeed * Time.deltaTime);
    }
}
