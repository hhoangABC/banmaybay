using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [SerializeField] protected GameObject playerBullet;
    [SerializeField] protected float bulletSpawnTime = 0.5f;
    [SerializeField] protected GameObject flash;
    [SerializeField] protected Transform spawnBullet;
    [SerializeField] protected Transform spawnBullet1;

    protected void Start()
    {
        flash.SetActive(false);
        StartCoroutine(Shoot());
    }

    protected void Fire()
    {
        Instantiate(playerBullet, spawnBullet.position, Quaternion.identity);
        Instantiate(playerBullet, spawnBullet1.position, Quaternion.identity);
    }

    protected IEnumerator Shoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(bulletSpawnTime);
            Fire();

            flash.SetActive(true);
            yield return new WaitForSeconds(0.05f);
            flash.SetActive(false);
        }
    }
}
