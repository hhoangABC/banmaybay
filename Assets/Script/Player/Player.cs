using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] protected float speed = 10;

    //Padding
    [SerializeField] protected float padding;
    protected float minX;
    protected float maxX;
    protected float minY;
    protected float maxY;

    protected void Start()
    {
        FindBoundaries();
    }

    protected void FindBoundaries()
    {
        Camera gameCamera = Camera.main;
        minX = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        maxX = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;
        minY = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        maxY = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
    }

    protected void Update()
    {
        float deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        float deltaY = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        float posX = Mathf.Clamp(transform.position.x + deltaX, minX, maxX);
        float posY = Mathf.Clamp(transform.position.y + deltaY, minY, maxY);

        transform.position = new Vector2(posX, posY);
    }
}
